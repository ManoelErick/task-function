from tokenize import Double, Floatnumber


print("***SOMA DA MÉDIA DO ALUNO***")

digNote1 = float(input("Digite a primeira nota: "))
digNote2 = float(input("Digite a segunda nota: "))

def sumAndDiv(note1, note2):
    sum = (note1 + note2) / 2
    return sum

finalAverage = sumAndDiv(digNote1, digNote2)

print("Média das notas: ", finalAverage)

if finalAverage >= 6:
    print("Aluno aprovado!")
else:
    print("Aluno Reprovado!")